﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains metadata for custom user data formats
    /// </summary>
    public sealed class UserFormat
    {
        public UserFormat(string name, string[] fields, int[] byteFormat)
        {
            Name = name;
            Fields = fields;
            ByteFormat = byteFormat;
        }

        public UserFormat(string name, int length, int value) :
            this(name, new string[length], new int[length])
        {
            for (int i = 0; i < length; i++)
                ByteFormat[i] = value;
        }

        public UserFormat(string name, int length) :
            this(name, new string[length], new int[length])
        { }

        public UserFormat() :
            this(null, null, null)
        { }

        public string Name { get; set; }
        public string[] Fields { get; set; }
        public int[] ByteFormat { get; set; }

        public override string ToString()
        {
            return String.Format("[{0}] Name({1}) Size(0x{2:X})",
                GetType().Name, Name, GetByteLength());
        }

        public int GetByteLength()
        {
            int result = 0;
            for (int i = 0; i < ByteFormat.Length; i++)
                result += ByteFormat[i];

            return result;
        }

        public void SetFields(int index, string name, int byteFormat)
        {
            Fields[index] = name;
            ByteFormat[index] = byteFormat;
        }

        public int Read(byte[] buffer, int index,
            out UserData result)
        {
            result = new UserData(this);
            index = result.FromBytes(buffer, index);

            return index;
        }

        public UserData Read(byte[] buffer, int index)
        {
            UserData result = new UserData(this);
            result.FromBytes(buffer, index);

            return result;
        }

        public int Write(byte[] buffer, int index, UserData value)
        {
            return value.ToBytes(buffer, index);
        }
    }
}