﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Indicates an address in SNES memory
    /// </summary>
    public struct LoRom :
        IEquatable<LoRom>,
        IWriteable
    {
        #region public static operators

        #region op ==

        public static bool operator ==(LoRom a, LoRom b)
        {
            return
                a.Bank == b.Bank &&
                a.Offset == b.Offset;
        }

        #endregion
        #region op !=

        public static bool operator !=(LoRom a, LoRom b)
        {
            return !(
                a.Bank == b.Bank &&
                a.Offset == b.Offset);
        }

        #endregion

        #endregion

        #region public constructors

        public LoRom(int address)
        {
            Bank = Addressing.GetBank(address);
            Offset = Addressing.GetOffset(address);
        }

        public LoRom(byte bank, ushort offset)
        {
            Bank = bank;
            Offset = offset;
        }

        #endregion
        #region public fields

        public byte Bank;
        public ushort Offset;

        #endregion
        #region public properties

        public int Address
        {
            get { return Addressing.GetPCAddress(Bank, Offset); }
        }

        #endregion
        #region public methods (object)

        public bool Equals(LoRom other)
        {
            return
                Bank == other.Bank &&
                Offset == other.Offset;
        }

        public override bool Equals(object obj)
        {
            return (obj is LoRom) &&
                ((LoRom)obj).Equals(this);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 27;
                hash = (17 * hash) + Bank.GetHashCode();
                hash = (17 * hash) + Offset.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return String.Format("[{0}] (${1:X2}:{2:X4})",
                GetType().Name, Bank, Offset);
        }

        #endregion
        #region public methods (IWriteable)

        public int ToBytes(byte[] buffer, int index)
        {
            ByteConverter.ToBytes.FromUInt16(buffer, index + 0x00, Offset);
            ByteConverter.ToBytes.FromByte(buffer, index + 0x02, Bank);

            return 0x03;
        }

        public int FromBytes(byte[] buffer, int index)
        {
            ByteConverter.FromBytes.ToUInt16(buffer, index + 0x00, out Offset);
            ByteConverter.FromBytes.ToByte(buffer, index + 0x02, out Bank);

            return 0x03;
        }

        #endregion
    }
}