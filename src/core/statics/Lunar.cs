﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains hooks and methods for "Lunar Compress.dll"
    /// </summary>
    public static class Lunar
    {
        public const uint MaxSize = 0x10000;
        public const uint Format1 = 0x04;
        public const uint Format2 = 0x00;

        public static readonly byte[] Buffer = new byte[Lunar.MaxSize];

        #region public static methods

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarVersion();

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern bool LunarOpenFile(string fileName, uint fileMode);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarOpenRAMFile(byte[] data, uint fileMode, uint size);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarSaveRAMFile(string fileName);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern bool LunarCloseFile();

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarGetFileSize();

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarReadFile(byte[] destination, uint size, uint address, uint seek);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarWriteFile(byte[] source, uint size, uint address, uint seek);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarSetFreeBytes(uint value);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarSNEStoPC(uint pointer, uint romType, uint header);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarPCtoSNES(uint pointer, uint romType, uint header);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarDecompress(byte[] destination, uint addressToStart, uint maxDataSize, uint format, uint format2, out uint lastRomPosition);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarRecompress(byte[] source, byte[] destination, uint dataSize, uint maxDataSize, uint format, uint format2);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern bool LunarEraseArea(uint address, uint size);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarExpandROM(uint mbits);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarVerifyFreeSpace(uint addressStart, uint addressEnd, uint size, uint bankType);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarIPSCreate(uint hwnd, string ipsFileName, string romFileName, string rom2FileName, uint flags);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarIPSApply(uint hwnd, string ipsFileName, string romFileName, uint flags);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern bool LunarCreatePixelMap(byte[] source, byte[] destination, uint numTiles, uint gfxType);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern bool LunarCreateBppMap(byte[] source, byte[] destination, uint numTiles, uint gfxType);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarSNEStoPCRGB(uint snesColor);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarPCtoSNESRGB(uint pcColor);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern bool LunarRender8x8(
            uint[] theMapBits,
            int theWidth, int theHeight, int displayAtX, int displayAtY,
            byte[] pixelMap, uint[] pcPalette, uint map8Tile, uint extra);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarWriteRatArea(byte[] theData, uint size, uint preferredAddress, uint minRange, uint maxRange, uint flags);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarEraseRatArea(uint address, uint size, uint flags);

        [System.Runtime.InteropServices.DllImport("Lunar Compress.dll")]
        public static extern uint LunarGetRatAreaSize(uint address, uint flags);

        #endregion
    }
}