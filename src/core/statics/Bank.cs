﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains predefined memory addresses for SNES LoRom banks
    /// </summary>
    public static class Bank
    {
        public const ushort Size = 0x8000;
        public const byte Minimum = 0x80;
        public const byte Maximum = 0xFF;

        #region $80 - $8F

        public const int x80 = 0x000000;
        public const int x81 = 0x008000;
        public const int x82 = 0x010000;
        public const int x83 = 0x018000;
        public const int x84 = 0x020000;
        public const int x85 = 0x028000;
        public const int x86 = 0x030000;
        public const int x87 = 0x038000;
        public const int x88 = 0x040000;
        public const int x89 = 0x048000;
        public const int x8A = 0x050000;
        public const int x8B = 0x058000;
        public const int x8C = 0x060000;
        public const int x8D = 0x068000;
        public const int x8E = 0x070000;
        public const int x8F = 0x078000;

        #endregion
        #region $90 - $9F

        public const int x90 = 0x080000;
        public const int x91 = 0x088000;
        public const int x92 = 0x090000;
        public const int x93 = 0x098000;
        public const int x94 = 0x0A0000;
        public const int x95 = 0x0A8000;
        public const int x96 = 0x0B0000;
        public const int x97 = 0x0B8000;
        public const int x98 = 0x0C0000;
        public const int x99 = 0x0C8000;
        public const int x9A = 0x0D0000;
        public const int x9B = 0x0D8000;
        public const int x9C = 0x0E0000;
        public const int x9D = 0x0E8000;
        public const int x9E = 0x0F0000;
        public const int x9F = 0x0F8000;

        #endregion
        #region $A0 - $AF

        public const int xA0 = 0x100000;
        public const int xA1 = 0x108000;
        public const int xA2 = 0x110000;
        public const int xA3 = 0x118000;
        public const int xA4 = 0x120000;
        public const int xA5 = 0x128000;
        public const int xA6 = 0x130000;
        public const int xA7 = 0x138000;
        public const int xA8 = 0x140000;
        public const int xA9 = 0x148000;
        public const int xAA = 0x150000;
        public const int xAB = 0x158000;
        public const int xAC = 0x160000;
        public const int xAD = 0x168000;
        public const int xAE = 0x170000;
        public const int xAF = 0x178000;

        #endregion
        #region $B0 - $BF

        public const int xB0 = 0x180000;
        public const int xB1 = 0x188000;
        public const int xB2 = 0x190000;
        public const int xB3 = 0x198000;
        public const int xB4 = 0x1A0000;
        public const int xB5 = 0x1A8000;
        public const int xB6 = 0x1B0000;
        public const int xB7 = 0x1B8000;
        public const int xB8 = 0x1C0000;
        public const int xB9 = 0x1C8000;
        public const int xBA = 0x1D0000;
        public const int xBB = 0x1D8000;
        public const int xBC = 0x1E0000;
        public const int xBD = 0x1E8000;
        public const int xBE = 0x1F0000;
        public const int xBF = 0x1F8000;

        #endregion
        #region $C0 - $CF

        public const int xC0 = 0x200000;
        public const int xC1 = 0x208000;
        public const int xC2 = 0x210000;
        public const int xC3 = 0x218000;
        public const int xC4 = 0x220000;
        public const int xC5 = 0x228000;
        public const int xC6 = 0x230000;
        public const int xC7 = 0x238000;
        public const int xC8 = 0x240000;
        public const int xC9 = 0x248000;
        public const int xCA = 0x250000;
        public const int xCB = 0x258000;
        public const int xCC = 0x260000;
        public const int xCD = 0x268000;
        public const int xCE = 0x270000;
        public const int xCF = 0x278000;

        #endregion
        #region $D0 - $DF

        public const int xD0 = 0x280000;
        public const int xD1 = 0x288000;
        public const int xD2 = 0x290000;
        public const int xD3 = 0x298000;
        public const int xD4 = 0x2A0000;
        public const int xD5 = 0x2A8000;
        public const int xD6 = 0x2B0000;
        public const int xD7 = 0x2B8000;
        public const int xD8 = 0x2C0000;
        public const int xD9 = 0x2C8000;
        public const int xDA = 0x2D0000;
        public const int xDB = 0x2D8000;
        public const int xDC = 0x2E0000;
        public const int xDD = 0x2E8000;
        public const int xDE = 0x2F0000;
        public const int xDF = 0x2F8000;

        #endregion
        #region $E0 - $EF

        public const int xE0 = 0x300000;
        public const int xE1 = 0x308000;
        public const int xE2 = 0x310000;
        public const int xE3 = 0x318000;
        public const int xE4 = 0x320000;
        public const int xE5 = 0x328000;
        public const int xE6 = 0x330000;
        public const int xE7 = 0x338000;
        public const int xE8 = 0x340000;
        public const int xE9 = 0x348000;
        public const int xEA = 0x350000;
        public const int xEB = 0x358000;
        public const int xEC = 0x360000;
        public const int xED = 0x368000;
        public const int xEE = 0x370000;
        public const int xEF = 0x378000;

        #endregion
        #region $F0 - $FF

        public const int xF0 = 0x380000;
        public const int xF1 = 0x388000;
        public const int xF2 = 0x390000;
        public const int xF3 = 0x398000;
        public const int xF4 = 0x3A0000;
        public const int xF5 = 0x3A8000;
        public const int xF6 = 0x3B0000;
        public const int xF7 = 0x3B8000;
        public const int xF8 = 0x3C0000;
        public const int xF9 = 0x3C8000;
        public const int xFA = 0x3D0000;
        public const int xFB = 0x3D8000;
        public const int xFC = 0x3E0000;
        public const int xFD = 0x3E8000;
        public const int xFE = 0x3F0000;
        public const int xFF = 0x3F8000;

        #endregion
    }
}