﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains common methods to manipulate addresses
    /// </summary>
    public static class Addressing
    {
        #region public static methods

        public static byte GetBank(int address)
        {
            return (byte)((address / Bank.Size) + Bank.Minimum);
        }

        public static ushort GetOffset(int address)
        {
            return (ushort)((address % Bank.Size) + Bank.Size);
        }

        public static void GetLoRom(int address,
            out LoRom result)
        {
            result.Bank = GetBank(address);
            result.Offset = GetOffset(address);
        }

        public static LoRom GetLoRom(int address)
        {
            return new LoRom(
                GetBank(address),
                GetOffset(address));
        }

        public static int GetPCAddress(int bank, int address)
        {
            return Bank.Size *
                ((byte)bank - Bank.Minimum) +
                ((ushort)address - Bank.Size);
        }

        public static int GetPCAddress(LoRom value)
        {
            return GetPCAddress(value.Bank, value.Offset);
        }

        #endregion
    }
}