﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains methods to manipulate raw data
    /// </summary>
    public static class Data
    {
        #region Data.Compress

        public static int Compress(byte[] buffer,
            out byte[] result)
        {
            uint length;
            {
                Lunar.LunarOpenRAMFile(buffer, 0, (uint)buffer.Length);
                length = Lunar.LunarRecompress(buffer, Lunar.Buffer, (uint)buffer.Length, (uint)Lunar.Buffer.Length, Lunar.Format1, Lunar.Format2);
                result = new byte[length];
                Array.Copy(Lunar.Buffer, 0, result, 0, length);
            }
            return (int)length;
        }

        public static byte[] Compress(byte[] buffer)
        {
            byte[] result;
            Compress(buffer, out result);
            return result;
        }

        public static int Compress(byte[] buffer, int index, byte[] values)
        {
            uint length;
            {
                Lunar.LunarOpenRAMFile(values, 0, (uint)values.Length);
                length = Lunar.LunarRecompress(values, Lunar.Buffer, (uint)values.Length, (uint)Lunar.Buffer.Length, Lunar.Format1, Lunar.Format2);
                Array.Copy(Lunar.Buffer, 0, buffer, index, length);
            }
            return (int)length;
        }

        #endregion
        #region Data.Decompress

        public static int Decompress(byte[] buffer, int index,
            out byte[] result)
        {
            uint length;
            {
                Lunar.LunarOpenRAMFile(buffer, 0, (uint)buffer.Length);
                length = Lunar.LunarDecompress(Lunar.Buffer, (uint)index, (uint)Lunar.Buffer.Length, Lunar.Format1, Lunar.Format2, out length);
                result = new byte[length];
                Array.Copy(Lunar.Buffer, 0, result, 0, length);
            }
            return (int)length;
        }

        public static byte[] Decompress(byte[] buffer, int index)
        {
            byte[] result;
            Data.Decompress(buffer, index, out result);
            return result;
        }

        #endregion
        #region Data.Matches

        public static bool Matches(byte[] source, int sourceIndex, byte[] target, int targetIndex, int count)
        {
            if (sourceIndex + count > source.Length) count = source.Length - sourceIndex;
            if (targetIndex + count > target.Length) count = target.Length - targetIndex;

            for (int i = 0; i < count; i++)
            {
                if (source[sourceIndex + i] != target[targetIndex + i])
                    return false;
            }

            return true;
        }

        public static bool Matches(byte[] source, int sourceIndex, byte[] target, int targetIndex)
        {
            return Matches(source, sourceIndex, target, targetIndex, source.Length - sourceIndex);
        }

        public static bool Matches(byte[] source, byte[] target)
        {
            return Matches(source, 0, target, 0, source.Length);
        }

        #endregion
    }
}