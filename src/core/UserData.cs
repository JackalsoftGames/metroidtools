﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains user-defined data
    /// </summary>
    public sealed class UserData :
        GameData
    {
        public UserData(UserFormat format)
        {
            if (format == null)
                throw new ArgumentNullException("format");

            Format = format;
            Values = null;
        }

        public UserFormat Format { get; private set; }
        public int[] Values;

        public override string ToString()
        {
            return String.Format("{0} Format({1})",
                base.ToString(), Format);
        }

        public override int ToBytes(byte[] buffer, int index)
        {
            base.ToBytes(buffer, index);
            int start = index;
            {
                index += ByteConverter.ToBytes.FromInt32Array(buffer, index, Format.ByteFormat, Values);
            }
            return (index - start);
        }

        public override int FromBytes(byte[] buffer, int index)
        {
            base.FromBytes(buffer, index);
            {
                index += ByteConverter.FromBytes.ToInt32Array(buffer, index, Format.ByteFormat, out Values);
            }
            return (index - Address);
        }
    }
}