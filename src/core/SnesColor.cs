﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Describes a SNES color in RGB-15 format
    /// </summary>
    public struct SnesColor :
        IEquatable<SnesColor>,
        IWriteable
    {
        public const ushort BitR = 0x001F;
        public const ushort BitG = 0x03E0;
        public const ushort BitB = 0x7C00;

        #region public static operators

        #region op SFML.c32

        public static implicit operator c32(SnesColor a)
        {
            return new c32(
                (byte)(a.R * 0x08),
                (byte)(a.G * 0x08),
                (byte)(a.B * 0x08),
                0xFF);
        }

        public static implicit operator SnesColor(c32 a)
        {
            return new SnesColor(
                (byte)(a.R / 0x08),
                (byte)(a.G / 0x08),
                (byte)(a.B / 0x08));
        }

        #endregion

        #region op ==

        public static bool operator ==(SnesColor a, SnesColor b)
        {
            return
                a.R == b.R &&
                a.G == b.G &&
                a.B == b.B;
        }

        #endregion
        #region op !=

        public static bool operator !=(SnesColor a, SnesColor b)
        {
            return !(
                a.R == b.R &&
                a.G == b.G &&
                a.B == b.B);
        }

        #endregion

        #endregion

        #region public constructors

        public SnesColor(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }

        public SnesColor(ushort value)
        {
            R = (byte)((value & BitR) >> 0x00);
            G = (byte)((value & BitG) >> 0x05);
            B = (byte)((value & BitB) >> 0x0A);
        }

        #endregion
        #region public fields

        public byte R;
        public byte G;
        public byte B;

        #endregion
        #region public methods (object)

        public bool Equals(SnesColor other)
        {
            return
                R == other.R &&
                G == other.G &&
                B == other.B;
        }

        public override bool Equals(object obj)
        {
            return (obj is SnesColor) &&
                ((SnesColor)obj).Equals(this);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 27;
                hash = (17 * hash) + R.GetHashCode();
                hash = (17 * hash) + G.GetHashCode();
                hash = (17 * hash) + B.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return String.Format("[{0}] R({1:X2}) G({2:X2}) B({3:X2})",
                GetType().Name, R, G, B);
        }

        #endregion
        #region public methods (IWriteable)

        public int ToBytes(byte[] buffer, int index)
        {
            ByteConverter.ToBytes.FromUInt16(buffer, index, (ushort)(
                (R << 0x00 & BitR) |
                (G << 0x05 & BitG) |
                (B << 0x0A & BitB)));

            return 0x02;
        }

        public int FromBytes(byte[] buffer, int index)
        {
            ushort value = ByteConverter.FromBytes.ToUInt16(buffer, index);
            {
                R = (byte)((value & BitR) >> 0x00);
                G = (byte)((value & BitG) >> 0x05);
                B = (byte)((value & BitB) >> 0x0A);
            }
            return 0x02;
        }

        #endregion
    }
}