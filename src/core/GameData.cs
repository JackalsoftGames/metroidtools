﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Base type for game data objects
    /// </summary>
    public abstract class GameData :
        IAddressable,
        IWriteable
    {
        public int Address { get; set; }
        
        public override string ToString()
        {
            return String.Format("[{0}] Address(${1:X2}:{2:X4})",
                GetType().Name,
                Addressing.GetBank(Address),
                Addressing.GetOffset(Address));
        }

        public virtual int ToBytes(byte[] buffer, int index)
        {
            return 0x00;
        }

        public virtual int FromBytes(byte[] buffer, int index)
        {
            Address = index;
            return 0x00;
        }
    }
}