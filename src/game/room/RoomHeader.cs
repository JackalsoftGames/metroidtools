﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools.Game
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains header data for rooms
    /// </summary>
    public sealed class RoomHeader :
        GameData
    {
        public byte Index;
        public byte Area;
        public byte X;
        public byte Y;
        public byte Width;
        public byte Height;
        public byte CameraUp;
        public byte CameraDown;
        public byte GraphicsFlag;
        public ushort Doors;

        public override string ToString()
        {
            return String.Format("{0} Area(0x{1:X2}) Index(0x{2:X2})",
                base.ToString(), Area, Index);
        }

        public override int ToBytes(byte[] buffer, int index)
        {
            base.ToBytes(buffer, index);
            int start = index;
            {
                index += ByteConverter.ToBytes.FromByte(buffer, index, Index);
                index += ByteConverter.ToBytes.FromByte(buffer, index, Area);
                index += ByteConverter.ToBytes.FromByte(buffer, index, X);
                index += ByteConverter.ToBytes.FromByte(buffer, index, Y);
                index += ByteConverter.ToBytes.FromByte(buffer, index, Width);
                index += ByteConverter.ToBytes.FromByte(buffer, index, Height);
                index += ByteConverter.ToBytes.FromByte(buffer, index, CameraUp);
                index += ByteConverter.ToBytes.FromByte(buffer, index, CameraDown);
                index += ByteConverter.ToBytes.FromByte(buffer, index, GraphicsFlag);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Doors);
            }
            return (index - start);
        }

        public override int FromBytes(byte[] buffer, int index)
        {
            base.FromBytes(buffer, index);
            {
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Index);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Area);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out X);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Y);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Width);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Height);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out CameraUp);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out CameraDown);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out GraphicsFlag);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Doors);
            }
            return (index - Address);
        }
    }
}