﻿/*
 * Metroid Tools
 * Author: Jackalsoft Games
 * Date: March 2020
 * 
 * This code is released under the GPL v3.0 license.
 * All other rights (c) their respective parties.
 */

namespace MetroidTools.Game
{
    using System;
    using System.Collections.Generic;

    using Squish;
    using Squish.Collections;
    using Squish.Extensions;

    using MetroidTools;
    using MetroidTools.Game;

    /// <summary>
    /// Contains header data for enemies
    /// </summary>
    public sealed class EnemyHeader :
      GameData
    {
        #region EnemyHeader.AISet

        /// <summary>
        /// Contains data for the enemy's AI behavior
        /// </summary>
        public struct AISet
        {
            public byte HurtTime;
            public ushort OnInit;
            public ushort OnMain;
            public ushort OnGrapple;
            public ushort OnHurt;
            public ushort OnFrozen;
            public ushort OnXray;
            public ushort OnPowerbomb;
            public ushort OnTouch;
            public ushort OnShot;
        }

        #endregion
        #region EnemyHeader.GraphicsSet

        /// <summary>
        /// Contains data for the enemy's graphics
        /// </summary>
        public struct GraphicsSet
        {
            public ushort TileDataSize;
            public ushort Palette;
            public ushort Sound;
            public ushort DeathAnimation;
            public LoRom TileData;
            public byte Layer;
        }

        #endregion
        #region EnemyHeader.MiscSet

        /// <summary>
        /// Contains data for unknown enemy parameters
        /// </summary>
        public struct MiscSet
        {
            public ushort Unknown1;
            public ushort Unknown2;

            public ushort Unused1;
            public ushort Unused2;
            public ushort Unused3;
            public ushort Unused4;
            public ushort Unused5;
        }

        #endregion
        #region EnemyHeader.VitalSet

        /// <summary>
        /// Contains data for the enemy's core data
        /// </summary>
        public struct VitalSet
        {
            public ushort Health;
            public ushort Damage;
            public ushort Width;
            public ushort Height;
            public ushort Drop;
            public ushort Weakness;
        }

        #endregion

        public AISet AI;
        public GraphicsSet Graphics;
        public MiscSet Misc;
        public VitalSet Vitals;

        public byte Bank;
        public ushort BossValue;
        public ushort Parts;
        public ushort Name;

        public override string ToString()
        {
            return String.Format("{0}",
              base.ToString());
        }

        public override int ToBytes(byte[] buffer, int index)
        {
            base.ToBytes(buffer, index);
            int start = index;
            {
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Graphics.TileDataSize);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Graphics.Palette);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Vitals.Health);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Vitals.Damage);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Vitals.Width);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Vitals.Height);
                index += ByteConverter.ToBytes.FromByte(buffer, index, Bank);
                index += ByteConverter.ToBytes.FromByte(buffer, index, AI.HurtTime);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Graphics.Sound);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, BossValue);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnInit);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Parts);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unused1);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnMain);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnGrapple);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnHurt);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnFrozen);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnXray);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Graphics.DeathAnimation);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unused2);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unused3);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnPowerbomb);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unknown1);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unused4);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unused5);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnTouch);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, AI.OnShot);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Misc.Unknown2);
                index += ByteConverter.Write(buffer, index, Graphics.TileData);
                index += ByteConverter.ToBytes.FromByte(buffer, index, Graphics.Layer);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Vitals.Drop);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Vitals.Weakness);
                index += ByteConverter.ToBytes.FromUInt16(buffer, index, Name);
            }
            return (index - start);
        }

        public override int FromBytes(byte[] buffer, int index)
        {
            base.FromBytes(buffer, index);
            {
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Graphics.TileDataSize);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Graphics.Palette);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Vitals.Health);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Vitals.Damage);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Vitals.Width);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Vitals.Height);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Bank);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out AI.HurtTime);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Graphics.Sound);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out BossValue);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnInit);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Parts);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unused1);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnMain);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnGrapple);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnHurt);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnFrozen);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnXray);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Graphics.DeathAnimation);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unused2);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unused3);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnPowerbomb);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unknown1);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unused4);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unused5);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnTouch);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out AI.OnShot);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Misc.Unknown2);
                index += ByteConverter.Read(buffer, index, out Graphics.TileData);
                index += ByteConverter.FromBytes.ToByte(buffer, index, out Graphics.Layer);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Vitals.Drop);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Vitals.Weakness);
                index += ByteConverter.FromBytes.ToUInt16(buffer, index, out Name);
            }
            return (index - Address);
        }
    }
}